#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:27467052:280b6c549287aabd4fad3142a41ffdeadfa219b5; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:25031976:91354e47f0c73e7b70016023e665310823276c5d EMMC:/dev/block/bootdevice/by-name/recovery 280b6c549287aabd4fad3142a41ffdeadfa219b5 27467052 91354e47f0c73e7b70016023e665310823276c5d:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
