ansi_cprng.ko crypto_ansi_cprng ansi_cprng crypto_stdrng stdrng

backlight.ko symbol:devm_backlight_device_unregister symbol:of_find_backlight_by_node symbol:backlight_force_update symbol:backlight_device_unregister symbol:backlight_register_notifier symbol:backlight_device_register symbol:devm_backlight_device_register symbol:backlight_unregister_notifier symbol:backlight_device_registered

br_netfilter.ko symbol:br_netfilter_enable

bus_timeout_mod.ko

cs_profile_mod.ko

evbug.ko input:b*v*p*e*_e*k*r*a*m*l*s*f*w*

generic_bl.ko
backlight

glink_test.ko symbol:glink_get_mock_debug_mask symbol:glink_get_mock_debug_mask

hrtimer_test_module.ko

ipc_logging_test.ko

ipi_test_module.ko

lcd.ko symbol:lcd_device_unregister symbol:devm_lcd_device_register symbol:lcd_device_register symbol:devm_lcd_device_unregister

ldrex_test_module.ko

mmc_block_test.ko
test_iosched

mmc_test.ko

modsign_test_module.ko

msm_bus_ioctl.ko

msm_iommu_test_module.ko

msm_ion_test_mod.ko

msm_watchdog_test_module.ko

page-nom.ko

pronto/pronto_wlan.ko

rdbg.ko

spidev.ko spi:spidev of:N*T*Cqcom,spi_msm_codec_slave* of:N*T*Crohm,dh2228fv*

spinlock_test_module.ko

test-iosched.ko symbol:test_iosched_add_wr_rd_test_req symbol:test_iosched_set_test_result symbol:test_iosched_free_test_req_data_buffer symbol:test_iosched_unregister symbol:test_iosched_create_test_req symbol:compare_buffer_to_pattern symbol:test_iosched_add_urgent_req symbol:test_iosched_register symbol:check_test_completion symbol:test_iosched_set_ignore_round symbol:test_iosched_add_unique_test_req symbol:test_iosched_mark_test_completion symbol:test_iosched_start_test

the_memory_prof_module.ko

tsensor_reset_mod.ko

ufs_test.ko symbol:ufs_test_init
test_iosched

wil6210.ko pci:v00001AE9d00000302sv*sd*bc*sc*i* pci:v00001AE9d00000310sv*sd*bc*sc*i*

wlan.ko

